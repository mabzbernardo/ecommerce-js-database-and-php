<?php 
	require "../partials/template.php";


	function get_title(){
		echo "Catalog";
	}

	function get_body_contents(){
		//require connection
	require "../controllers/connection.php";
	?>
		<h1 class="text-center py-5">Bag Items</h1>

		<!-- Items List -->
		<div class="row">
			<!-- open php to publish items -->
			<?php 
				//publish items, need to connect to database via mysqli
				$items_query = "SELECT * FROM items"; 
				$items = mysqli_query($conn, $items_query); // catch result;mysqli_query(connection details,)
				// var_dump($items);
				// die();
				foreach ($items as $indiv_item){
					// var_dump($indiv_item); //lalabas ung details ng item
				?>
					<div class="col-lg-4 py-2">
						<div class="card">
							<img class="card-img-top" height="200px" src="<?php echo $indiv_item['imgPath'] ?>">
							<div class="card-body">
								<h4 class="card-title"><?= $indiv_item['name']?></h4>
								<p class="card-text">Price: PHP <?= $indiv_item['price']?></p>
								<p class="card-text">Description: <?= $indiv_item['description']?></p>
								<p class="card-text">Category: 
									<?php 
										$catId = $indiv_item['category_id'];
										// var_dump($catId);

										$category_query = "SELECT * FROM categories WHERE id = $catId";
										// var_dump($category_query);
										// <!-- 1 row - need to transform to associative array -->
										$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
										// var_dump($category);

											echo $category['name'];

									 ?>

								</p>
							</div>
							<div class="card-footer">
								<a href="../controllers/delete-item-process.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete Item:</a>
								<a href="edit-item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-success">Edit Item</a>
							</div>
							<div class="footer">
								<!-- <form action="../controllers/add-to-cart-process.php" method="POST">  hindi magsusubmit-->
									<!-- <input type="hidden" name="id" value="<?php echo $indiv_item['id'] ?>"> alisin dhil walang form-->
									<input type="number" name="cart" class="form-control" value="1">
									<button type="button" class="btn btn-primary addToCart" data-id="<?php echo $indiv_item['id']?>">Add To Cart</button>									
								<!-- </form> -->
							</div>
						</div>
					</div>
				<?php
				}
			?>
		</div>
		<script type="text/javascript" src="../assets/scripts/add-to-cart.js"></script>
	<?php

	}

?>